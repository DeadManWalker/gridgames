games.snake
===========

.. automodule:: games.snake
    :autosummary:
    :show-inheritance:
    :members:
    :member-order: bysource
    :private-members:
    :special-members: __init__