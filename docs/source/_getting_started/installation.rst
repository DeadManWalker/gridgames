Installation
============

Only a few steps to get ``GridGames`` up and running:

#. Download the `repository <https://bitbucket.org/DeadManWalker/gridgames/src/master/>`_

    .. code-block:: console

        >>> git clone https://bitbucket.org/DeadManWalker/gridgames/src/master/
        >>> cd gridgames

#. Install the required python packages
    I recommend using a `virtualenv <https://pypi.org/project/virtualenv/>`_ to
    keep everything self-contained

    .. code-block:: console

        >>> pip install -r requirements.txt

#. Try some games
    .. code-block:: console

        >>> ./play --help

