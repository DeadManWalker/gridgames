Documentation
=============


Generate Documentation
----------------------

Run the Sphinx auto documentation tool:

.. code-block:: console

    >>> cd gridgames/docs
    >>> make html

The documentation is servered in ``gridgames/docs/buid/html/`` and
can thus be locally viewed in a browser:

.. code-block:: console

    >>> firefox gridgames/docs/build/html/index.html


Write Documentation
-------------------

This package solely documents itself with `Google Style <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>`_
Python `docstrings <https://www.python.org/dev/peps/pep-0257/>`_. Just stick to the convention.

For type information this package makes use of the Python :mod:`typing` module.
To ensure thorough type information, please annotate

- Function and method parameters and return object
- Class and instance variables
- Global variables


Type annotations
^^^^^^^^^^^^^^^^

To reference types before they are defined, use

    >>> from __future__ import annotations

at the beginning of a module. This allows to e.g. reference the class name in the
classes definition.

Examples:

    >>> from __future__ import annotations
    >>>
    >>>
    >>> # Uses the ``Placeholder`` type as type annotation even though
    >>> # the class has not been defined yet.
    >>> # Although the function could also have been defined after the class
    >>> # to solve this issue, the order may matter to emphasize module
    >>> # features that should be used or considered fist.
    >>> def createNested(num: int): -> Placeholder
    >>>     """Create ``num`` nested :class:`Placeholder` s."""
    >>>     last = Placeholder()
    >>>     for _ in range(num-1):
    >>>         last = Placeholder(last)
    >>>     return last
    >>>
    >>>
    >>> # Uses the class type within its class definition.
    >>> class Placeholder(object):
    >>>     """Placeholder object"""
    >>>     def __init__(self, other_placeholder: Placeholder=None): -> None
    >>>         self.other: Placeholder = other_placeholder
