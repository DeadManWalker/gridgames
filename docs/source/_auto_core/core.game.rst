core.game
=========

.. automodule:: core.game
    :autosummary:
    :show-inheritance:
    :members:
    :member-order: bysource
    :private-members:
    :special-members: __init__