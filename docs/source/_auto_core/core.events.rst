core.events
===========

.. automodule:: core.events
    :autosummary:
    :show-inheritance:
    :members:
    :member-order: bysource
    :private-members:
    :special-members: __init__