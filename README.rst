GridGames
=========

A collection of small, 2d grid games, with an event-driven, component-based framework.

For a full documentation see /docs/build/html/index.html or at `ReadTheDocs <https://gridgames.readthedocs.io/en/latest/>`_