#!/usr/bin/env python3

import argparse
import re
from functools import partial

from core import events, game, keymapper as km
from core.scheduler import Scheduler

from components.loader import TileLoader
from components.cmd import CmdDrawer, CmdInput
from components.tk import TkDrawer, TkInput

import games




def setKeyBindings(game):
    km.bindKeys([km.KEY_Q, km.ESCAPE], game.exit)

def getDefaultSize(size, default_size, prefer_square=True):
    width, height = size
    try:
        d_width, d_height = default_size
    except TypeError:
        d_width = d_height = int(default_size)

    if prefer_square:
        if width is None:
            width = height
        if height is None:
            height = width

    if width is None:
        width = d_width
    if height is None:
        height = d_height

    return (width, height)


def getArgs(game_list):
    def checkSize(value):
        try:
            size = int(value)
            return (size, size)
        except ValueError:
            pass
        match = re.match(r"([0-9]+)[^0-9]([0-9]+)", value)
        if not match:
            raise argparse.ArgumentTypeError(f"'{value}' is an invalid size string")
        w, h = match.groups()
        return (int(w), int(h))

    parser = argparse.ArgumentParser()
    parser.add_argument("game", choices=game_list, help="Game to play")
    parser.add_argument("-ww", "--width", type=int, help="Width of game area")
    parser.add_argument("-hh", "--height", type=int, help="Height of game area")
    parser.add_argument("-ss", "--size", type=checkSize, help="Size of game area. Use '<width_and_height>' (e.g. --ss '20') or \
                                                        '<width><delimiter char><height>' (e.g. --ss '15x20')")
    parser.add_argument("-s", "--speed", type=float, help="Game speed in seconds")
    parser.add_argument("--gui", help="Play in GUI instead of cmd", action="store_true")

    return parser.parse_args()


def createTetris(size):
    size = getDefaultSize(size, (12, 20), prefer_square=False)
    tetris = games.tetris.Tetris(size)
    tetris.addComponents([TileLoader])

    km.bindKeys([km.KEY_A, km.ARROW_LEFT], tetris.moveLeft)
    km.bindKeys([km.KEY_D, km.ARROW_RIGHT], tetris.moveRight)
    km.bindKeys([km.KEY_W, km.ARROW_UP], tetris.rotateRight)
    km.bindKeys([km.KEY_S, km.ARROW_DOWN], tetris.pullGameloop)

    return tetris

def createSnake(size):
    size = getDefaultSize(size, 10)
    snake = games.snake.Snake(size)

    km.bindKeys([km.KEY_A, km.ARROW_LEFT], snake.moveLeft)
    km.bindKeys([km.KEY_D, km.ARROW_RIGHT], snake.moveRight)
    km.bindKeys([km.KEY_W, km.ARROW_UP], snake.moveUp)
    km.bindKeys([km.KEY_S, km.ARROW_DOWN], snake.moveDown)

    return snake

def createGOL(size):
    size = getDefaultSize(size, 50)
    gol = games.gol.GameOfLife(size)
    return gol



def exit(game):
    game.exit()
    Scheduler.stopLoop()


if __name__ == "__main__":
    game_list = {
        "tetris"    : createTetris,
        "snake"     : createSnake,
        "gol"       : createGOL
    }


    width, height = None, None
    speed = None
    components = []

    args = getArgs(list(game_list.keys()))
    if args.width:
        width = args.width
    if args.height:
        height = args.height
    if args.size:
        width, height = args.size
    if args.speed:
        speed = args.speed*1000
    if args.gui:
        components += [TkInput, TkDrawer]
    else:
        components += [CmdInput, CmdDrawer]



    game_inst = game_list[args.game]( (width, height) )
    game_inst.addComponents(components)
    if speed is not None:
        game_inst.speed = speed
    setKeyBindings(game_inst)


    events.find(game.EVENTS.ON_GAME_LOST, game.EVENTS.ON_GAME_EXITED)\
            .connect(partial(exit, game_inst), no_args=True)

    game_inst.start()

    try:
        Scheduler.startLoop()
    except KeyboardInterrupt:
        pass
    finally:
        exit(game_inst)


