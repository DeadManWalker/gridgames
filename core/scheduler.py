"""This module provides the :class:`Scheduler` for handling delayed and intervalic callables.

Examples:

    >>> from core.scheduler import Scheduler
    >>>
    >>>
    >>> class CountDown(object):
    >>>     '''Start a count down from any number'''
    >>>     def __init__(self, start: int=10):
    >>>         self.start: int = start + 1
    >>>         self.current: int = self.start
    >>>         self.NAME = "countdown"
    >>>         self.FINISHED_NAME = "countdown_finished"
    >>>
    >>>     def start(self) -> CountDown:
    >>>         Scheduler.runInterval(self._countDown, interval_ms=1000, name=self.NAME)
    >>>         return self
    >>>
    >>>     def stop(self) -> None:
    >>>         Scheduler.remove(self.NAME)
    >>>
    >>>     def reset(self) -> None:
    >>>         self.current = self.start
    >>>         # Scheduler.edit with default arguments pulls the execution to now
    >>>         Scheduler.edit(self.NAME)
    >>>
    >>>     def _countDown(self) -> None:
    >>>         '''Count one down and print status'''
    >>>         self.current -= 1
    >>>         print(f"Count Down: {self.current}")
    >>>         if self.current <= 0:
    >>>             # Stop count down interval and print finished message after another second
    >>>             self.stop()
    >>>             Scheduler.runOnce(self._finished, delay_ms=1000, name=self.FINISHED_NAME)
    >>>
    >>>     def _finished(self):
    >>>         print("Count Down is finished")
    >>>
"""
from __future__ import annotations
from typing import *
import time


class Scheduler(object):
    """The :class:`Scheduler` schedules delayed and intervalic callables to be executed in the future"""
    _running = False
    _strict = False
    _schedule = {}

    @classmethod
    def _addSchedule(cls, time, func, interval_ms, name):
        cls._schedule[time] = cls._schedule.get(time, []) + [(func, interval_ms, name)]


    @classmethod
    def runOnce(cls, func: Callable, delay_ms: int=0, name: Optional[str]=None) -> None:
        """Schedule a ``Callable`` to run once with a delay.
        
        If a name is given, the scheduled execution can later be edited or removed.

        Args:
            func: Callable to run once with a delay
            delay_ms: Optional delay in milliseconds
            name: Optional name of this scheduled execution to reference for editing or removing
        """
        if delay_ms == 0:
            func()
        else:
            cls._addSchedule(time.time()*1000+delay_ms, func, False, name)

    @classmethod
    def runInterval(cls, func: Callable, interval_ms: int=0, delay_ms: int=0,
                    name: Optional[str]=None) -> None:
        """Schedule a ``Callable`` to run in an interval.

        If a name is given, the scheduled execution can later be edited or removed.

        Args:
            func: Callable to run in an interval
            interval_ms: Milliseconds in between intervals. Defaults to 0, making
                it run at every update step of the :class:`Scheduler`
            delay_ms: Optional delay in milliseconds to start the interval
            name: Optional name of this scheduled execution to reference for editing or removing
        """
        cls._addSchedule(time.time()*1000+delay_ms, func, interval_ms, name)


    @classmethod
    def remove(cls, name: str) -> None:
        """Remove a scheduled execution by name.

        This removes all scheduled executions with the given name.

        Args:
            name: Name of the scheduled execution to remove
        """
        for t, f_i_n in cls._schedule.items():
            for i, (func, ms, fname) in enumerate(f_i_n[:]):
                if name == fname:
                    del f_i_n[i]

    @classmethod
    def edit(cls, name: str, interval_ms: Optional[int]=None, delay_ms: int=0) -> bool:
        """Reschedule a scheduled execution by name.

        The interval and start delay can be edited. Only the first scheduled execution with this
        name will be edited.

        Args:
            name: Name of the scheduled execution to edit
            interval_ms: Milliseconds in between intervals. Defaults to None, not editing this property
            delay_ms: Optional, initial delay in milliseconds


        Returns:
            Whether a scheduled execution with this name was found and edited
        """
        name_func = None
        name_ms = 0
        event_found = False
        for t, f_i_n in cls._schedule.items():
            for i, (func, interval_ms, fname) in enumerate(f_i_n[:]):
                if name == fname:
                    name_func = func
                    name_ms = interval_ms
                    del f_i_n[i]
                    event_found = True
                    break
            if event_found:
                break

        interval_ms = name_ms if interval_ms is None else interval_ms
        if event_found:
            if interval_ms is False:
                cls.runOnce(name_func, delay_ms=delay_ms, name=name)
            else:
                cls.runInterval(name_func, interval_ms=interval_ms, delay_ms=delay_ms, name=name)
        return event_found


    @classmethod
    def stopLoop(cls, strict: bool=False) -> None:
        """Stop the blocking scheduler loop.

        Args:
            strict: Whether the schedule should be cleared. Defaults to ``False``
        """
        cls._running = False
        cls._strict = strict
        if strict:
            cls._schedule.clear()

    @classmethod
    def startLoop(cls) -> None:
        """Start the blocking scheduler loop by calling :meth:`updateStep` in a loop"""
        cls._running = True
        while cls._running:
            cls.updateStep()
        cls._schedule.clear()


    @classmethod
    def updateStep(cls) -> None:
        """Process the schedule once.

        This should be called repeatedly, either by calling :meth:`startLoop` once, or
        directly for non-blocking needs. The smaller the calling interval, the greater the
        scheduler accuracy.
        """
        t = time.time()*1000
        for schedule_t, f_i_n in tuple(cls._schedule.items()):
            if t < schedule_t:
                continue
            cls._schedule.pop(schedule_t, False)
            for func, ms, name in f_i_n:
                if not cls._running and cls._strict:
                    return
                func()
                if ms is False:
                    continue
                if ms > 0:
                    cls._addSchedule(t+ms, func, ms, name)
                else:
                    cls._addSchedule(0, func, 0, name)
