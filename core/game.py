"""This modules provides the base class :class:`Game` and its events.

Subclass :class:`Game` to provide a common game interface for external
components to hook into.
"""
from __future__ import annotations
from typing import *
from typing_extensions import Literal

import numpy as np

from core import events
from core.events import Event
from core.scheduler import Scheduler
from components import Component


class EVENTS(object):
    """Container for game event name constants."""
    ON_GAME_STARTED                 = "onGameStarted"
    ON_GAME_EXITED                  = "onGameExited"
    ON_GAME_LOST                    = "onGameLost"
    ON_GAME_WON                     = "onGameWon"
    ON_GAME_POINTS_UPDATED          = "onGamePointsUpdated"
    ON_GAME_SPEED_UPDATED           = "onGameSpeedUpdated"
    ON_GAME_UPDATED                 = "onGameUpdated"
    ON_GAME_GRAPHICS_UPDATE_NEEDED  = "onGameGraphicsUpdateNeeded"


WidthHeight = Tuple[int, int]

class Game(object):
    """Base class for games to inherit and extend."""
    onStarted: Event                = events.create(EVENTS.ON_GAME_STARTED)
    onExited: Event                 = events.create(EVENTS.ON_GAME_EXITED)
    onLost: Event                   = events.create(EVENTS.ON_GAME_LOST)
    onWon: Event                    = events.create(EVENTS.ON_GAME_WON)
    onPointsUpdated: Event          = events.create(EVENTS.ON_GAME_POINTS_UPDATED)
    onSpeedUpdated: Event           = events.create(EVENTS.ON_GAME_SPEED_UPDATED)
    onUpdated: Event                = events.create(EVENTS.ON_GAME_UPDATED)
    onGraphicsUpdateNeeded: Event   = events.create(EVENTS.ON_GAME_GRAPHICS_UPDATE_NEEDED)


    def __init__(self, width_height: Sequence[int], speed_ms: int=1000):
        """Initialize properties and connect events.

        Args:
            width_height: width and height of the game field
            speed_ms: game update speed in milliseconds. Defaults to 1000

        Example:
            Instatiate a Game object::

                >>> mygame = Game((20, 10), speed_ms=500)
        """
        self._size = tuple(width_height)
        self._speed = speed_ms
        self._points = 0
        self._lost = False
        self._exited = False
        self._won = False
        self._graphics_update_needed = False
        self.components: List[Component] = [] #: List of components added by :meth:`addComponents`


    @property
    def size(self) -> WidthHeight:
        """Size of the game field as (width, height)."""
        return self._size

    @property
    def width(self) -> int:
        """Width of game field."""
        return self.size[0]

    @property
    def height(self) -> int:
        """Height of game field."""
        return self.size[1]


    @property
    def speed(self) -> int:
        """Game update speed in milliseconds."""
        return self._speed

    @speed.setter
    def speed(self, speed_ms: int) -> None:
        """Set game update speed and emit EVENTS.ON_GAME_SPEED_UPDATED."""
        self._speed = speed_ms
        self.onSpeedUpdated.emit(speed=speed_ms)


    @property
    def points(self) -> int:
        """Game points."""
        return self._points

    @points.setter
    def points(self, points: int) -> None:
        """Set game points and emit EVENTS.ON_GAME_POINTS_UPDATED."""
        self._points = points
        self.onPointsUpdated.emit(points=points)


    @property
    def field(self) -> np.ndarray:
        """Game field as a 2d numpy array of ``size``."""
        field = np.zeros((self.height, self.width), dtype=int)
        return field

    @property
    def lost(self) -> bool:
        """Game was lost."""
        return self._lost

    @property
    def exited(self) -> bool:
        """Game was exited."""
        return self._exited

    @property
    def won(self) -> bool:
        """Game was won."""
        return self._won

    @property
    def running(self) -> bool:
        """Game is actively running"""
        return (not self.lost and not self.exited and not self.won)

    @property
    def graphics_update_needed(self) -> bool:
        """Graphics need to be updated."""
        return self._graphics_update_needed

    @graphics_update_needed.setter
    def graphics_update_needed(self, needed: bool) -> None:
        """Set whether the graphics need to be updated.

        If graphics need to be updated, :meth:`graphicsUpdateNeeded` is called.

        Args:
            needed: Whether the graphics need to be updated
        """
        if not needed:
            self._graphics_update_needed = False
        else:
            self.graphicsUpdateNeeded()


    def addComponents(self, components: Iterable[Union[type, Component]]) -> None:
        """Add components to the list of components.

        Take an iterable of component types or instances, instantiate
        if necessary and add them to :py:attr:`components`.
        This is useful if
        you don't need to touch a component and therefore let the game
        instantiate it.
        It also servers as a common storage for components to live.

        Example:
            Add components to a game::

                >>> from components.cmd import CmdInput, CmdDrawer

                >>> cmdinput = CmdInput(mygame)
                >>> cmdinput.store_this_int = 42
                >>> mygame.addComponents((cmdinput, CmdDrawer))

        Args:
            components: Component types or instances to add/ instantiate
        """
        for comp in components:
            if isinstance(comp, type):
                self.components.append(comp(self))
            else:
                self.components.append(comp)

    def start(self) -> None:
        """Start game and emit signal EVENTS.ON_GAME_STARTED."""
        self._lost = False
        self._exited = False
        self._won = False
        self.onStarted.emit()

    def exit(self) -> None:
        """Exit game if not already exited and emit EVENTS.ON_GAME_EXITED."""
        if not self.exited:
            self._exited = True
            self.onExited.emit()

    def lose(self) -> None:
        """Lose game if not already lost and emit EVENTS.ON_GAME_LOST."""
        if not self.lost:
            self._lost = True
            self.onLost.emit()

    def win(self) -> None:
        """Win game if not already lost and emit EVENTS.ON_GAME_WON."""
        if not self.won:
            self._won = True
            self.onWon.emit()

    def update(self) -> None:
        """Update game if not already lost or exited and emit EVENTS.ON_GAME_UPDATED."""
        if self.running:
            self.onUpdated.emit()



    def graphicsUpdateNeeded(self) -> None:
        """Notify that the graphics need to be updated and emit
        EVENTS.ON_GAME_GRAPHICS_UPDATE_NEEDED.
        """
        self._graphics_update_needed = True
        self.onGraphicsUpdateNeeded.emit()


    def startGameloop(self) -> None:
        """Start the gameloop which schedules :meth:`update` to be called every :py:attr:`speed` milliseconds."""
        Scheduler.runInterval(self.update, self.speed, name="gameloop")

    def stopGameloop(self) -> None:
        """Stop the gameloop."""
        Scheduler.remove("gameloop")

    def pullGameloop(self) -> None:
        """Call :meth:`update` immediately."""
        Scheduler.edit("gameloop", interval_ms=self.speed)

    def connectEvents(self) -> None:
        """Connect events required for the game to function

        Connections:
            ON_GAME_STARTED                 => self.startGameloop
            ON_GAME_SPEED_UPDATED           => self.pullGameloop
            ON_GAME_LOST, ON_GAME_EXITED    => self.stopGameloop
        """
        self.onStarted.connect(self.startGameloop, no_args=True)
        self.onSpeedUpdated.connect(self.pullGameloop, no_args=True)
        events.find(self.onLost, self.onExited).connect(self.stopGameloop, no_args=True)


