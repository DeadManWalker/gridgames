"""This module provides the basic functionality for event-driven programming.


Events are objects that can :meth:`EventInterface.emit` data to functions that
have :meth:`EventInterface.connect` ed themselves to the event. It is the same
principle of the publish-subscriber pattern: The event object is the publisher
and publishes through :meth:`EventInterface.emit`, while the connecting functions
are the subscriber, subscribing through :meth:`EventInterface.connect`.

Instead of creating and finding events directly, use the provided
helper functions:

- :func:`find`
- :func:`create`

They take care of a fhandling already existing or not yet created events:

    >>> on_jumped1 = events.create("onJumped")
    >>> on_jumped2 = events.create("onJumped")
    >>> on_jumped1 is on_jumped2
    True
    >>> on_jumped3 = events.create("onJumped", strict=True)
    KeyError: "Event 'onJumped' already exists"


    >>> on_seen = events.find("onSeen")
    >>> on_seen.awaiting
    True
    >>> events.create("onSeen", strict=True)
    >>> on_seen.awaiting
    False


    >>> print_hello = lambda: print("Hello :)")
    >>>
    >>> events.find("onGreeted").connect(print_hello)
    >>> events.find("onGreeted", strict=True).connect(print_hello, no_args=True)
    KeyError: "Event 'onGreeted' not found"
    >>> events.create("onGreeted").emit()
    Hello :)



Examples:
    
    >>> import events
    >>>
    >>>
    >>> class SpaceCounter(object):
    >>>     '''Count how many characters are spaces'''
    >>>     def __init__(self):
    >>>         self.spaces = 0
    >>>         # Connect the ``increment`` method to the ``onCharProcessed``
    >>>         # event, even though the event might not exist yet
    >>>         events.find("onCharProcessed").connect(self.increment)
    >>>
    >>>     def increment(self, event):
    >>>         if event["char"] == " ":
    >>>             self.spaces += 1
    >>>
    >>> # Create a SpaceCounter
    >>> space_counter = SpaceCounter()
    >>>
    >>>
    >>> def logEmitterOnce(event):
    >>>     '''Log the event name on first emit'''
    >>>     e = event["event"]
    >>>     with open("input.log", "a") as f:
    >>>         f.write(f"Emitter: {e.name}\\n")
    >>>     e.disconnect(logEmitterOnce)
    >>>
    >>> def printProgress():
    >>>     '''Print a progress symbol for every character processed'''
    >>>     print(".-")
    >>>
    >>>
    >>> # Create a new event or take an existing event instance
    >>> onCharProcessed = events.create("onCharProcessed")
    >>>
    >>> # Connect to event object
    >>> onCharProcessed.connect(logEmitterOnce)
    >>> # Connect to event object and get called with no arguments
    >>> onCharProcessed.connect(printProgress, no_args=True)
    >>>
    >>> CHARACTERS = "Oh hey there" # string to be processed
    >>>
    >>> for char in CHARACTERS:
    >>>     onProcessedChar.emit(char=char)
    >>>
    >>> # make sure ``CHARACTERS`` really contains 2 spaces
    >>> asset space_counter.spaces == 2
    >>>
    >>> '''
    >>> ``printProgress`` will print:
    >>>     .-.-.-.-.-.-.-.-.-.-.-.-
    >>> ``input.log`` will say:
    >>>     Emitter onCharProcessed
    >>> '''


.. note::
    The following classes (and funcitons) should not be used directly if possible,
    but rather the helper functions :func:`find` and :func:`create`.
"""
from __future__ import annotations
from typing import *
from typing_extensions import Literal
from collections.abc import Collection
from abc import ABC, abstractclassmethod


# Custom module types
NameOrEvent = Union[str, "Event"]
FalseOrEvent = Union[Literal[False], "Event"]
EventDict = Dict[str, "Event"]
ConnectionCallable = Union[ Callable[[], Any], Callable[[Mapping], Any] ]
ConnectionCallables = Sequence[ConnectionCallable]
FalseOrConnectionCallable = Union[Literal[False], ConnectionCallable]





def find(name_or_event: NameOrEvent, *names_or_events: Sequence[NameOrEvent],
         strict: bool=False) -> Union[Event, EventsProxy]:
    """Find one or more events by name or object.

    Finds each event using :func:`findSingle`.
    If multiple events are returned, they will be wrapped inside an :class:`EventsProxy` object.


    Args:
        name_or_event: Event object or name to be found
        *name_or_event: Optional, additional event objects
            or names to be found
        strict: Strict mode requires every event to be found, otherwise a ``KeyError``
            is raised. Defautls to ``False``

    Returns:
        Event object found or :class:`EventsProxy` for multiple events

    Raises:
        KeyError: If ``strict`` is ``True`` and an event is not found
    """
    names_or_events += (name_or_event, )
    if len(names_or_events) > 1:
        return EventsProxy([findSingle(n, strict) for n in names_or_events])
    return findSingle(names_or_events[0], strict)



def create(name: str, *names: Sequence[str], strict: bool=False) -> Union[Event, EventsProxy]:
    """Create one or more events.

    Creates each event using :func:`createSingle`.
    If multiple events are returned, they will be wrapped inside an :class:`EventsProxy` object.

    Args:
        name: Event name
        *names: Optional, additional event names
        strict: Strict mode requires no other event to exist with one of the name.
            Defaults to ``False``

    Returns:
        Event object created or :class:``EventsProxy`` for multiple events

    Raises:
        KeyError: If ``strict`` is ``True`` and another event with one of the names already exists
    """
    names += (name, )
    if len(names) > 1:
        return EventsProxy([createSingle(n, strict) for n in names])
    return createSingle(names[0], strict)




class EventInterface(ABC):
    """Event interface that an event class must implement (inherit) to enforce
    the three event methods: :func:`connect`, :func:`disconnect`, :func:`emit`.
    """

    @abstractclassmethod
    def connect(self, *args, **kwargs):
        """Connect (a) function(s) to this event."""

    @abstractclassmethod
    def disconnect(self, *args, **kwargs):
        """Disonnect (a) connected function(s) from this event."""

    @abstractclassmethod
    def emit(self, *args, **kwargs):
        """Emit to all connections."""



class Event(EventInterface):
    """``Event`` implements the :class:`EventInterface`. It stores each created event
    in its class dictionary, keyed by the given name. Event objects of this
    class can be queried and removed by the provided class methods.

    Examples:

        >>> def printName(ev: Event): -> None
        >>>     print(f"Hello {ev['name']} :)")
        >>>     ev.disconnect(printName)
        >>>
        >>> on_connected = Event("onConnected")
        >>> on_connected.connect(printName)
        >>>
        >>> new_name = input("Type in your name: ")
        Type in your name: Kevin
        >>>
        >>> on_connected.emit(name=new_name)
        Hello Kevin :)
    """

    _events: EventDict = {}

    def __init__(self, name: str, awaiting_owner: bool=False) -> None:
        """Initialize a ``Event`` object with a name

        If the event was not created by the intended owner (the one to emit the event),
        ``awaiting_owner`` should be set to ``True``. Once the intended owner is ready, he should
        then set it to ``False``.

        Args:
            name: Unique name of the event
            awaiting_owner: Whether the intended owner created this event or is still awaited
        """
        self.connections: Dict[ConnectionCallable, bool] = {} #: Stores connected functions with ``no_args`` parameter
        self.awaiting: bool = awaiting_owner #: Whether the intended owner created this event or is still awaited
        self._name: str = name
        self._data = {}
        self.add(name, self)

    @classmethod
    def add(cls, name: str, event: "Event") -> None:
        """Add the event to the class dictionary, keyed by the name."""
        cls._events[name] = event

    @classmethod
    def remove(cls, name_or_event: NameOrEvent) -> FalseOrEvent:
        """Remove the event from the class dictionary, by event name or event object."""
        name = name_or_event
        if isinstance(name_or_event, cls):
            name = name_or_event.name
        return cls._events.pop(name, False)

    @classmethod
    def get(cls, name_or_event: NameOrEvent) -> FalseOrEvent:
        """Get the event by event name or event object.
        Return False if not found."""
        if isinstance(name_or_event, cls):
            if name_or_event in cls._events.values():
                return name_or_event
            return False
        return cls._events.get(name_or_event, False)


    @property
    def name(self) -> str:
        """Return the event name."""
        return self._name

    @property
    def data(self) -> Dict:
        """Return the event data."""
        return self._data


    def emit(self, **event_data) -> None:
        """Emit to all connections.

        Connected functions that accept arguments are called with this event object. The new
        event data is assigned to ``data``.
        Items of ``data`` can be retrieved directly from the event object:

            >>> ev = events.create("ev")
            >>> ev.emit(test="data", very="creative")
            >>> ev["test"]
            'data'
            >>> ev["test"] is ev.data["test"]
            True

        Args:
            **event_data: Any key-value data that connected functions get called with
        """
        self._data = event_data
        for con, no_args in tuple(self.connections.items()):
            if con not in self.connections:
                return
            if no_args:
                con()
            else:
                con(self)

    def connect(self, con: ConnectionCallable, *cons: ConnectionCallables,
                no_args: bool=False) -> None:
        """Connect at least one function to this event.

        Args:
            con: Function to connect
            *cons: Optional, additional functions to connect
            no_args: Call the function(s) with no parameters. Defaults to False, therefore calling the
                functions with the event data dictionary
        """
        for c in cons + (con, ):
            self.connections[c] = no_args

    def disconnect(self, con: ConnectionCallable , *cons: ConnectionCallables) -> FalseOrConnectionCallable:
        """Disconnect at least one function from this event.

        Args:
            con: Function to disconnect
            *cons: Optional, additional functions to disconnect

        Returns:
            The callable(s) if found, otherwise False. If more than one callable has been
            passed, a list of them is returned.
        """
        if len(cons) == 0:
            return self.connections.pop(con, False) != False
        return [(self.connections.pop(c, False) != False) for c in cons + (con, )]


    def __getitem__(self, item):
        """Get an item of ``data``"""
        return self.data[item]

    def __repr__(self):
        return f"{self.__class__.__name__}(name='{self.name}', awaiting_owner={self.awaiting})"



class EventsProxy(Collection, EventInterface):
    """Collection of events and proxy to perform simultaneous event methods on it.

    Example:
        Perform an event method on a group of events::

        >>> EventsProxy((event1, event2, event2)).connect(func1, func2)
    """
    def __init__(self, events: Iterable[EventInterface]):
        """Initialize an :class:``EventsProxy`` object containing events.

        Args:
            events: ``Iterable`` of event objects
        """
        self.events: Iterable[EventInterface] = events #: Store event objects

    def connect(self, *args, **kwargs) -> None:
        [e.connect(*args, **kwargs) for e in self.events]

    def disconnect(self, *args, **kwargs) -> None:
        return [e.disconnect(*args, **kwargs) for e in self.events]

    def emit(self, *args, **kwargs) -> None:
        [e.emit(*args, **kwargs) for e in self.events]


    def __iter__(self):
        return iter(self.events)

    def __contains__(self, item):
        return item in self.events

    def __len__(self):
        return len(self.events)

    def __repr__(self):
        return f"{self.__class__.__name__}(events={self.events})"




def findSingle(name_or_event: NameOrEvent, strict: bool) -> Event:
    """Find an event by name or object.

    If the event is not found, a ``KeyError`` is raised for ``strict`` equals ``True``, otherwise
    a corresponding :class:`Event` object is created and returned. Events are therefore
    lazy-initialized on first need.

    Args:
        name_or_event: :class:`Event` object or name to be found
        strict: Strict mode requires the event to be found, otherwise a ``KeyError``
            is raised

    Returns:
        Event object found (or created)

    Raises:
        KeyError: If ``strict`` is ``True`` and the event is not found
    """
    event = Event.get(name_or_event)
    if event:
        return event
    if strict:
        raise KeyError(f"Event '{name_or_event}' not found")
    try:
        name = name_or_event.name
    except AttributeError:
        name = name_or_event
    return Event(name, awaiting_owner=True)



def createSingle(name: str, strict: bool) -> Event:
    """Create an event.

    If an event with this name already exists, a ``KeyError`` is raised for ``strict`` equals ``True``,
    otherwise the existing event gets returned.

    Args:
        name: Event name
        strict: Strict mode requires no other event to exist with this name

    Returns:
        Event object created or found

    Raises:
        KeyError: If ``strict`` is ``True`` and another event with this name already exists
    """
    event = Event.get(name)
    if not event:
        return Event(name)
    if event.awaiting:
        event.awaiting = False
        return event
    if strict:
        raise KeyError(f"Event '{name}' already exists")

    return event

