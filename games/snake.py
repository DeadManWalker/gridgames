import random
from collections import deque

import numpy as np

from core import events, game



class EVENTS(object):
    pass


class Snake(game.Game):
    def __init__(self, *args, speed_ms=100, **kwargs):
        self.snake = deque()
        self.items = []
        self.directions = deque()
        super().__init__(*args, speed_ms=speed_ms, **kwargs)
        self.connectEvents()

    onCollision             = events.create("onCollision")
    onSelfCollision         = events.create("onSelfCollision")
    onBoundaryCollision     = events.create("onBoundaryCollision")
    onMoved                 = events.create("onGameMoved")
    onDirectionChanged      = events.create("onGameDirectionChanged")
    onItemConsumed          = events.create("onGameItemConsumed")
    onGrown                 = events.create("onGameGrown")


    @property
    def field(self):
        field = super().field
        for x, y in self.snake:
            field[y, x] = 1
        for x, y in self.items:
            field[y, x] = 8
        return field

    @property
    def direction(self):
        if self.directions:
            self._direction = self.directions[0]
        return self._direction

    @direction.setter
    def direction(self, dir):
        dx, dy = dir
        _dx, _dy = self.direction
        # Current and new direction are identical or opposite of each other
        if abs(dx) == abs(_dx) and abs(dy) == abs(_dy):
            return
        self.directions.append( (dx, dy) )
        self.onDirectionChanged.emit(direction=self.direction)

    @property
    def snake_head(self):
        try:
            return self.snake[-1]
        except IndexError:
            return None

    @property
    def snake_tail(self):
        try:
            return self.snake[0]
        except IndexError:
            return None


    def spawnSnake(self):
        x, y = random.randint(0, self.width-1), random.randint(0, self.height-1)
        self.snake.clear()
        self.snake.append( (x, y) )
        self.directions.clear()
        self.directions.append( (0, 0) )

    def spawnItem(self):
        ys, xs = np.where(self.field == 0)
        x, y = random.choice(xs), random.choice(ys)
        self.items.append( (x, y) )


        
    def moveLeft(self):
        self.direction = (-1, 0)

    def moveRight(self):
        self.direction = (1, 0)

    def moveUp(self):
        self.direction = (0, -1)

    def moveDown(self):
        self.direction = (0, 1)


    def move(self):
        if self.lost:
            return

        success = self._tryMove(self.direction)
        self.onMoved.emit(success=success, move=self.direction)

    def update(self):
        if not self.running:
            return
        self.move()
        self.checkItemConsumed()
        self.checkWon()
        super().update()


    def checkBoundaryCollision(self, pos):
        hx, hy = pos
        if hx < 0 or hx >= self.width or hy < 0 or hy >= self.height:
            self.onBoundaryCollision.emit(coords=(hx, hy))
            return ([hx], [hy])
        return [], []
    
    def checkSelfCollision(self, pos):
        pos = tuple(pos)
        if pos != self.snake_head and pos in self.snake:
            self.onSelfCollision.emit(coords=pos)
            return ([pos[0]], [pos[1]])
        return [], []
            
    def isSnakeColliding(self, pos):
        xs, ys = np.hstack( (self.checkBoundaryCollision(pos), self.checkSelfCollision(pos)) )
        
        if len(xs) > 0:
            self.onCollision.emit(coords=tuple(zip(xs, ys)))
            return True
        return False


    def checkWon(self):
        if np.all(self.field):
            self.win()

    def checkItemConsumed(self):
        for item in self.items[:]:
            if item == self.snake_head:
                self.grow()
                self.items.remove(item)
                self.points += 1
                self.onItemConsumed.emit(item=item)
                return item

    def grow(self):
        self.snake.appendleft(self.snake_tail)
        self.spawnItem()
        self.onGrown.emit(size=len(self.snake))


    def _tryMove(self, direction):
        pos = self.snake[-1][0] + direction[0], self.snake[-1][1] + direction[1]
        if self.isSnakeColliding(pos):
            return False
        if self.directions:
            self.directions.popleft()
        self.snake.popleft()
        self.snake.append(pos)
        return True

    def connectEvents(self):
        self.onStarted.connect(self.spawnSnake, self.spawnItem, no_args=True)
        events.find(self.onStarted, self.onMoved).connect(
            self.onGraphicsUpdateNeeded.emit, no_args=True)
        self.onCollision.connect(self.lose, no_args=True)
        super().connectEvents()


