import random
from itertools import permutations

import numpy as np

from core import events, game



class EVENTS(object):
    pass


class GameOfLife(game.Game):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._field = super().field
        self.connectEvents()


    @property
    def field(self):
        return self._field[:]


    def randomizeField(self, alive_chance=0.5):
        self._field = np.array(np.random.random_sample(super().field.shape) + alive_chance, dtype=int)

    def clearField(self):
        self._field = super().field

    def placeShape(self, shape2d, pos=None):
        shape2d = np.array(shape2d)
        shape_h, shape_w = shape2d.shape
        if pos is None:
            x, y = (self.width//2 - shape_w//2, self.height//2 - shape_h//2)
        else:
            x, y = pos
        self._field[y:y+shape_h, x:x+shape_w] += shape2d


    def neighbors(self, pos):
        px, py = pos
        return (
            (x+px, y+py) for (x, y) in
            set(permutations( (-1, -1, 0, 1, 1), 2))
            if 0 <= x+px < self.width and 0 <= y+py < self.height
        )

    def neighborsAlive(self, pos):
        return sum( ((self.field[y, x] > 0) for (x, y) in self.neighbors(pos)) )

    def aliveByRules(self, pos):
        alive = self._field[pos[1], pos[0]]
        alive_n = self.neighborsAlive(pos)

        # Dead cell with 3 living neighbors gets reborn
        if not alive:
            if alive_n == 3:
                return True
            return False

        # Living cell with 2 or 3 living neighbors stays alive
        if alive_n in  (2, 3):
            return True

        # Rest dies
        return False

    def updateGeneration(self):
        new_field = super().field
        for y, x in np.ndindex(self._field.shape):
            new_field[y, x] = int(self.aliveByRules( (x, y) ))
        self._field = new_field


    def update(self):
        if self.running:
            self.updateGeneration()
        super().update()


    def connectEvents(self):
        self.onStarted.connect(self.randomizeField, no_args=True)
        events.find(self.onStarted, self.onUpdated).connect(
            self.onGraphicsUpdateNeeded.emit, no_args=True)
        super().connectEvents()


