import numpy as np
import random
from functools import partial

from core import events, game



class EVENTS(object):
    ON_TETRIS_TILE_SPAWNED      = "onTetrisTileSpawned"
    ON_TETRIS_TILE_REGISTERED   = "onTetrisTileRegistered"
    ON_TETRIS_ROTATED           = "onTetrisRotated"
    ON_TETRIS_MOVED             = "onTetrisMoved"
    ON_TETRIS_ROWS_CLEARED      = "onTetrisRowsCleared"
    ON_TETRIS_TILE_BAKED        = "onTetrisTileBaked"
    ON_TETRIS_COLLISION         = "onTetrisCollision"
    ON_TETRIS_TILES_COLLISION   = "onTetrisTilesCollision"
    ON_TETRIS_COLLISION         = "onTetrisCollision"
    ON_TETRIS_BOUNDARY_COLLISION= "onTetrisBoundaryCollision"


class Tile(object):
    def __init__(self, name, mask, worldpos=None, rotation=0):
        self.name = name
        self._mask = np.array(mask)
        self._rot_mask = mask[:]
        self.worldpos = worldpos
        self.rotation = rotation

    @property
    def size(self):
        return len(self.mask[0]), len(self.mask)

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, val):
        self._rotation = val
        self._rot_mask = self._mask[:]
        rot_count = val // 90
        for _ in range(rot_count):
            self._rot_mask = np.rot90(self._rot_mask)

    @property
    def mask(self):
        return self._rot_mask

    def __copy__(self):
        return type(self)(self.name, self.mask)




class Tetris(game.Game):
    def __init__(self, *args, **kwargs):
        self._tile = None
        self._tiles = {}
        self.tile_filter = None

        super().__init__(*args, **kwargs)
        self.connectEvents()
        self._field = super().field

    onTileSpawned       = events.create(EVENTS.ON_TETRIS_TILE_SPAWNED)
    onTileRegistered    = events.create(EVENTS.ON_TETRIS_TILE_REGISTERED)
    onRotated           = events.create(EVENTS.ON_TETRIS_ROTATED)
    onMoved             = events.create(EVENTS.ON_TETRIS_MOVED)
    onRowsCleared       = events.create(EVENTS.ON_TETRIS_ROWS_CLEARED)
    onTileBaked         = events.create(EVENTS.ON_TETRIS_TILE_BAKED)
    onCollision         = events.create(EVENTS.ON_TETRIS_COLLISION)
    onTilesCollision    = events.create(EVENTS.ON_TETRIS_TILES_COLLISION)
    onBoundaryCollision = events.create(EVENTS.ON_TETRIS_BOUNDARY_COLLISION)


    @property
    def field(self):
        p = self.tile.worldpos
        tw, th = self.tile.size
        field = np.copy(self._field)
        field[p[1]:p[1]+th, p[0]:p[0]+tw] += self.tile.mask 
        return field

    @property
    def tiles(self):
        return self._tiles

    @property
    def tile(self):
        return self._tile


    def registerTile(self, name, mask):
        """ Register a new tile by a unique name and its 2d mask """
        self._tiles[name] = mask
        self.onTileRegistered.emit(name=name, mask=mask)


    def exit(self):
        self._tile = None
        super().exit()

    def lose(self):
        self._tile = None
        super().lose()



    def rotateRight(self):
        self._rotate(90)
        
    def moveLeft(self):
        self._move([-1, 0])

    def moveRight(self):
        self._move([1, 0]) 

    def moveDown(self):
        self._move([0, 1])


    def spawnTile(self):
        spawn_pos = [self.size[0]//2, 0]
        if self.tile_filter is None:
            names = self._tiles.keys()
        else:
            names = (name for name, mask in self._tiles.items() if self.tile_filter(name, mask))
        name = random.choice(list(names))
        self._tile = Tile(name, self._tiles[name], worldpos=spawn_pos)
        self.onTileSpawned.emit(tile=self.tile)

    def tryClearRows(self):
        filled = self._field.all(axis=1)
        num_filled = sum(filled)
        self._field = np.vstack((np.zeros((num_filled, self.size[0])), self._field[~filled]))
        self.points += num_filled
        self.onRowsCleared.emit(rows=np.where(filled == True)[0])

    def bakeTile(self):
        p = self.tile.worldpos
        tw, th = self.tile.size
        self._field[p[1]:p[1]+th, p[0]:p[0]+tw] += self.tile.mask
        self.onTileBaked.emit()


    def checkBoundaryCollision(self, tile):
        px, py = tile.worldpos
        tw, th = tile.size
        fw, fh = self.size
        
        l, r, t, b = px, px+tw, py, py+th
        yxs = [[], []]
        if l < 0:
            yxs = np.hstack( (yxs, np.where(tile.mask[:, :-l] > 0)) )
        elif r > fw:
            yxs = np.hstack( (yxs, np.where(tile.mask[:, fw-r:] > 0)) )
        if t < 0:
            yxs = np.hstack( (yxs, np.where(tile.mask[:-t, :] > 0)) )
        elif b > fh:
            yxs = np.hstack( (yxs, np.where(tile.mask[fh-b:, :] > 0)) )
        ys, xs = yxs  
        xs, ys = (np.array(xs)+px, np.array(ys)+py)
        if len(xs) > 0:
            self.onBoundaryCollision.emit(coords=tuple(zip(xs, ys)))
        return (xs, ys)

    def checkTilesCollision(self, tile):
        px, py = tile.worldpos
        tw, th = tile.size
        fw, fh = self.size

        t, b, l, r = py, py+th, px, px+tw
        st, sb, sl, sr = max(0, t), min(b,fh), max(0, l), min(r,fw)
        ys, xs = np.where(np.add(self._field[st:sb, sl:sr], tile.mask[st-t:th+sb-b, sl-l:tw+sr-r]) > 1)
        xs, ys = xs+px, ys+py
        if len(xs) > 0:
            self.onTilesCollision.emit(coords=tuple(zip(xs, ys)))
        return (xs, ys)

    def isTileColliding(self, tile):
        xs, ys = np.hstack( (self.checkBoundaryCollision(tile), self.checkTilesCollision(tile)) )
        
        if len(xs) > 0:
            self.onCollision.emit(coords=tuple(zip(xs, ys)))
            return True
        return False

    def checkLost(self):
        if self.tile.worldpos[1] == 0 and self.isTileColliding(self.tile):
            self.lose()
            return True
        return False
    


    def _rotate(self, degrees):
        if self._lost:
            return
        success = self._tryMotion( rotation=((self.tile.rotation + degrees) % 360) )
        self.onRotated.emit(success=success, degrees=degrees)

    def _move(self, direction):
        if self._lost:
            return
        success = self._tryMotion( worldpos=np.add(self.tile.worldpos, direction) )
        self.onMoved.emit(success=success, move=tuple(direction))

    def _tryMotion(self, rotation=None, worldpos=None):
        rotation = self.tile.rotation if rotation is None else rotation
        worldpos = self.tile.worldpos if worldpos is None else worldpos
        tile = Tile(self.tile.name, self._tiles[self.tile.name], \
                    rotation=rotation, worldpos=worldpos)
        if self.isTileColliding(tile):
            return False
        self._tile = tile
        return True


    def connectEvents(self):
        self.onStarted.connect(     self.spawnTile, no_args=True)
        self.onTileSpawned.connect( self.checkLost, no_args=True)
        self.onUpdated.connect(     self.moveDown, no_args=True)
        self.onMoved.connect(       self._updateField)
        events.find(self.onStarted, self.onMoved, self.onRotated).connect(
            self.onGraphicsUpdateNeeded.emit, no_args=True)
        super().connectEvents()

    def _updateField(self, event):
        if event["success"]:
            return

        if event["move"][1] == 1:  
            self.bakeTile()  
            self.tryClearRows()  
            self.spawnTile()


