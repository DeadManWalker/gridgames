import tkinter as tk
import numpy as np
from functools import partial


from components import Component
from core import events, game, keymapper as km
from core.scheduler import Scheduler



root = None
gui = None


class _TkGui(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.canvas = None
        self.createWidgets()
        self.pack(expand=True, fill=tk.BOTH)

    def createWidgets(self):
        self.canvas = tk.Canvas(self, bd=0, bg="black")
        self.canvas.pack(expand=True, fill=tk.BOTH)

        tkUpdate()



class TkDrawer(Component):
    def __init__(self, game):
        super().__init__(game)   
        tkInit()
        self._setupConnections()
        self.map = {
            0: "white",
            1: "blue",
            2: "gray",
            4: "black"
        }
        self._tile_size = None
        self.setTileSize()

    onDrawn = events.create("onDrawn")
    onResized = events.create("onResized")

    def _setupConnections(self):
        events.find(game.EVENTS.ON_GAME_STARTED).connect(self.connect, no_args=True)
        events.find(game.EVENTS.ON_GAME_LOST).connect(self.disconnect, no_args=True)
        self.onResized.connect(self.setTileSize, no_args=True)
        root.bind("<Configure>", self.onConfigured)

    def connect(self):
        events.find(game.EVENTS.ON_GAME_GRAPHICS_UPDATE_NEEDED).connect(self.draw)

    def disconnect(self):
        events.find(game.EVENTS.ON_GAME_GRAPHICS_UPDATE_NEEDED).disconnect(self.draw)
        self.lost()

    @property
    def tile_size(self):
        return self._tile_size

    def setTileSize(self, size=None):
        if size is None:
            expand = 0.8
            self._tile_size = min(root.winfo_width()*expand//self.game.size[0], \
                root.winfo_height()*expand//self.game.size[1])
        else:
            self._tile_size = size

    def onConfigured(self, event):
        wscale = float(event.width) / root.winfo_width()
        hscale = float(event.height) / root.winfo_width()
        if wscale == hscale == 1:
            return
        scale = min(wscale, hscale)
        self.onResized.emit(scale=scale)
        
        
    def draw(self, e):
        g = self.game
        combined = g.field
        filled_row = np.ones(combined.shape[1])*2
        combined = np.vstack((combined, filled_row))
        filled_col = np.ones((combined.shape[0], 1))*4
        grid = np.hstack((filled_col, combined, filled_col))
        
        w, h = len(grid[0]), len(grid)
        char_hframe = "-"
        char_vframe = " "
        frame_len = 2*len(char_vframe) + 2*1 + len(str(g.points))
        padding =  " " * int(w - frame_len/2)
        topbottom = padding + char_hframe * (frame_len // len(char_hframe))
        middle = padding + char_vframe + " " + str(g.points) + " " + char_vframe

        gui.canvas.delete(tk.ALL)
        for y, row in enumerate(grid):
            for x, cell in enumerate(row):
                try:
                    val = self.map[cell]
                except KeyError:
                    val = [self.map[v] for v in self.map.keys() if (v & cell == v)]
                tx, ty = x*self.tile_size, y*self.tile_size
                bx, by = tx+self.tile_size, ty+self.tile_size
                gui.canvas.create_rectangle(tx, ty, bx, by, fill=self.map[cell])

        self.onDrawn.emit()

    def lost(self):
        pass

    def exit(self):
        gui.destroy()



class TkInput(Component):
    KEY_MAP = {
        "Up"        : km.ARROW_UP,
        "Right"     : km.ARROW_RIGHT,
        "Down"      : km.ARROW_DOWN,
        "Left"      : km.ARROW_LEFT,

        "Escape"    : km.ESCAPE
        #TODO: To be continued
    }

    def __init__(self, game, *args, **kwargs):
        tkInit()
        self.game = game
        self._raw = self._nonblocking = None
        self._setupConnections()

        super().__init__(self.game, *args, **kwargs)

    onKeyInput = events.create("onKeyInput")


    def _setupConnections(self):
        events.find(game.EVENTS.ON_GAME_STARTED).connect(
            partial(root.bind, "<Key>", self.onKeyPressed),
            no_args=True
        )


    def onKeyPressed(self, event):
        key = event.keysym
        if len(key) == 1:
            key = key.upper()
            code = ord(key)
        else:
            try:
                code = self.KEY_MAP[key]
            except KeyError:
                return
            key = ""
        self.onKeyInput.emit(code=code, key=key)





def tkInit(_root=None, _gui=None):
    global root, gui
    if isTkInit():
        return False
 
    if _root is None:
        root = tk.Tk()
    else:
        root = _root
    if _gui is None:
        gui = _TkGui(master=root)
    elif isinstance(_gui, type):
        gui = _gui(master=root)
    else:
        gui = _gui
    Scheduler.runInterval(tkUpdate, name="tkUpdate")
    return True


def tkUpdate():
    root.update_idletasks()
    root.update() 

def isTkInit():
    return (root is not None and gui is not None)    
    


