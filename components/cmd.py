
import numpy as np
import fcntl
import os   
from functools import partial
from abc import ABC, abstractmethod

from components import Component
from core import events, game, keymapper as km
from core.scheduler import Scheduler

try:
    # Windows
    import msvcrt
except ImportError:
    try:
        # Linux
        import sys, termios, tty
    except ImportError:
        pass



class CmdDrawer(Component):
    """ Draws grid games on the cmd """
    def __init__(self, game):
        super().__init__(game)   
        self.clearing = 50
        self.map = {
            0: "- ",
            1: "x ",
            2: "ZZ",
            4: "II",
            8: "o "
        }

    onDrawn = events.create("onDrawn")

    def connectEvents(self):
        events.find(game.EVENTS.ON_GAME_STARTED).connect(self._connect, no_args=True)
        events.find(game.EVENTS.ON_GAME_LOST).connect(self._disconnect, no_args=True)
        super().connectEvents()

    def _connect(self):
        events.find(game.EVENTS.ON_GAME_GRAPHICS_UPDATE_NEEDED).connect(self.draw)

    def _disconnect(self):
        events.find(game.EVENTS.ON_GAME_GRAPHICS_UPDATE_NEEDED).disconnect(self.draw)
        self.lost()
        

    def draw(self, e):
        print("DRAWING")
        g = self.game
        combined = g.field
        filled_row = np.ones(combined.shape[1])*2
        combined = np.vstack((combined, filled_row))
        filled_col = np.ones((combined.shape[0], 1))*4
        grid = np.hstack((filled_col, combined, filled_col))
        
        w, h = len(grid[0]), len(grid)
        char_hframe = "-"
        char_vframe = " "
        frame_len = 2*len(char_vframe) + 2*1 + len(str(g.points))
        padding =  " " * int(w - frame_len/2)
        topbottom = padding + char_hframe * (frame_len // len(char_hframe))
        middle = padding + char_vframe + " " + str(g.points) + " " + char_vframe

        for i in range(self.clearing):
            print()

        print(topbottom)
        print(middle)
        print(topbottom)
        print()

        for row in grid:
            for cell in row:
                try:
                    val = self.map[cell]
                except KeyError:
                    val = [self.map[v] for v in self.map.keys() if (v & cell == v)]
                print(self.map[cell], end="")     
            print()

        self.onDrawn.emit()

    def lost(self):
        print("YOU LOST")





class _BaseCmdKeyGetter(ABC):
    """ Abstract Base class for any CmdKeyGetter implementation. The only task
        is to return a pressed key on the cmd in a non-blocking way. The 
        provided methods will be called from CmdInput """
    @abstractmethod
    def getKeyCode(self):
        """ Return a tuple of (<key>, <code>) if a key was pressed.
            Otherwise return None """
        pass

    def exit(self):
        """ Gets called by CmdInput on exit  """
        pass


class _LinuxCmdKeyGetter(_BaseCmdKeyGetter):
    """ CmdKeyGetter implementation for Linux Cmds """
    class RawStream(object):
        """ Context manager for starting and ending a stream in raw mode """
        def __init__(self, stream):
            self.stream = stream
            self.fd = self.stream.fileno()

        def enter(self):
            self.original_stty = termios.tcgetattr(self.stream)
            tty.setcbreak(self.stream)
            return self

        def exit(self):
            termios.tcsetattr(self.stream, termios.TCSANOW, self.original_stty)
            return self

        def __enter__(self):
            self.enter()

        def __exit__(self, *args):
            self.exit()


    class NonblockingStream(object):
        """ Context manager for starting and ending a stream in nonblocking mode """
        def __init__(self, stream):
            self.stream = stream
            self.fd = self.stream.fileno()

        def enter(self):
            self.orig_fl = fcntl.fcntl(self.fd, fcntl.F_GETFL)
            fcntl.fcntl(self.fd, fcntl.F_SETFL, self.orig_fl | os.O_NONBLOCK)
            return self

        def exit(self):
            fcntl.fcntl(self.fd, fcntl.F_SETFL, self.orig_fl)
            return self

        def __enter__(self):
            self.enter()

        def __exit__(self, *args):
            self.exit()

    ARROWS      = {
        "A" : km.ARROW_UP,
        "C" : km.ARROW_RIGHT,
        "B" : km.ARROW_DOWN,
        "D" : km.ARROW_LEFT
    }

    def __init__(self):
        self._setupCmd()

    def getKeyCode(self):
        keys = []
        while True:
            k = sys.stdin.read(1)
            if not k:
                break
            keys.append(k)
        if len(keys) == 1:
            key = keys[0].upper()
            code = ord(key)
        elif len(keys) == 3:
            code = self.ARROWS[keys[2]]
            key = ""
        else:
            return
        return (key, code)

    def exit(self):
        self._restoreCmd()

    def _setupCmd(self):
        self._raw = self.RawStream(sys.stdin).enter()
        self._nonblocking = self.NonblockingStream(sys.stdin).enter()

    def _restoreCmd(self):
        self._raw.exit()
        self._nonblocking.exit()


class _WindowsCmdKeyGetter(_BaseCmdKeyGetter):
    """ CmdKeyGetter implementation for Windows Cmds """
    @abstractmethod
    def getKeyCode(self):
        pass
        # TODO: Needs to be implemented, by SOMEONE WHO IS ON WINDOWS        
        
        #if msvcrt.kbhit():
        #    msvcrt.getch().decode('utf-8')



class CmdInput(Component):
    """ Fetches pressed keys on the cmd and emits them as an event """
    def __init__(self, game, *args, cmd_key_getter=None, **kwargs):
        self.game = game
        self.cmd_key_getter = None
        self.setCmdKeyGetter(cmd_key_getter)
        self._setupConnections()

        super().__init__(self.game, *args, **kwargs)

    onKeyInput = events.create("onKeyInput")

    def setCmdKeyGetter(self, cmd_key_getter=None):
        """ Set the CmdKeyGetter if given, otherwise initialize the default,
            platform dependent CmdKeyGetter """
        if cmd_key_getter:
            self.cmd_key_getter = cmd_key_getter
            return True
        # windows
        if os.name == "nt":
            raise NotImplementedError("CmdInput found no CmdKeyGetter for Windows")
            #self.cmd_key_getter = _WindowsCmdKeyGetter()
        else:
            self.cmd_key_getter = _LinuxCmdKeyGetter()
        return True

    def _setupConnections(self):
        Scheduler.runInterval(self._eventloop)

    def _eventloop(self):
        key_code = self.cmd_key_getter.getKeyCode()
        if key_code is not None:                               
            self.onKeyInput.emit(key=key_code[0], code=key_code[1])

    def exit(self):
        self.cmd_key_getter.exit()



