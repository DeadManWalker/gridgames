"""components"""
from core import events, game


class EVENTS(object):
    ON_COMPONENT_CREATED    = "onComponentCreated"


class Component(object):
    """ Base component """
    def __init__(self, _game):
        self.game = _game
        self.connectEvents()
        events.create(EVENTS.ON_COMPONENT_CREATED).emit(component=self)

    def connectEvents(self):
        events.find(game.EVENTS.ON_GAME_EXITED).connect(self.exit, no_args=True)

    def exit(self):
        pass
