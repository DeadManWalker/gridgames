import numpy as np
import json

from components import Component




class TileLoader(Component):
    def __init__(self, game):
        super().__init__(game)
        self.loadDict(_TILES)
        self.game.tile_filter = lambda name, mask: np.array(mask).sum() == 4

    def loadJson(self, tiles_json):
        with open(tiles_json) as f:
            tiles = json.load(f)
            self.loadDict(tiles)

    def loadDict(self, tiles_dict):
        for name, mask in tiles_dict.items():
            self.game.registerTile(name, mask)
        




_TILES = {
    "4O": [
            [1, 1],
            [1, 1]
    ],
    "4I": [
            [1, 1, 1, 1]
    ],
    "4L": [
            [1, 0],
            [1, 0],
            [1, 1]
    ],
    "4J": [
            [0, 1],
            [0, 1],
            [1, 1]
    ],
    "4S": [
            [0, 1, 1],
            [1, 1, 0]
    ],
    "4Z": [
            [1, 1, 0],
            [0, 1, 1]
    ],
    "4K": [
            [0, 1, 0],
            [1, 1, 1]
    ],

    "5I": [
        [1, 1, 1, 1, 1]
    ],
    "5L": [
        [1, 0],
        [1, 0],
        [1, 0],
        [1, 1]
    ],
    "5J": [
        [0, 1],
        [0, 1],
        [0, 1],
        [1, 1]
    ],
    "5P": [
        [1, 1],
        [1, 1],
        [1, 0]
    ],
    "5q": [
        [1, 1],
        [1, 1],
        [0, 1]
    ],
    "5C": [
        [1, 1],
        [1, 0],
        [1, 1]
    ],
    "5k": [
        [1, 0],
        [1, 0],
        [1, 1],
        [1, 0]
    ],
    "5r": [
        [1, 0],
        [1, 1],
        [1, 0],
        [1, 0]
    ],
    "5+": [
        [0, 1, 0],
        [1, 1, 1],
        [0, 1, 0]
    ],
    "5S": [
        [0, 1, 1],
        [0, 1, 0],
        [1, 1, 0]
    ],
    "5Z": [
        [1, 1, 0],
        [0, 1, 0],
        [0, 1, 1]
    ],
    "5T": [
        [1, 1, 1],
        [0, 1, 0],
        [0, 1, 0]
    ],
    "5EL": [
        [1, 1, 0],
        [0, 1, 1],
        [0, 1, 0]
    ],
    "5EJ": [
        [0, 1, 1],
        [1, 1, 0],
        [0, 1, 0]
    ]
}
